# Spanish translations for kcm_bluetooth.po package.
# Copyright (C) 2020 This file is copyright:
# This file is distributed under the same license as the bluedevil package.
#
# Automatically generated, 2020.
# SPDX-FileCopyrightText: 2020, 2021, 2022, 2023, 2024 Eloy Cuadra <ecuadra@eloihr.net>
msgid ""
msgstr ""
"Project-Id-Version: kcm_bluetooth\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-02-18 00:38+0000\n"
"PO-Revision-Date: 2024-02-18 19:42+0100\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Eloy Cuadra"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "ecuadra@eloihr.net"

#: bluetooth.cpp:102
#, kde-format
msgctxt "DeviceName Network (Service)"
msgid "%1 Network (%2)"
msgstr "Red %1 (%2)"

#: ui/Device.qml:70 ui/main.qml:237
#, kde-format
msgid "Disconnect"
msgstr "Desconectar"

#: ui/Device.qml:70 ui/main.qml:237
#, kde-format
msgid "Connect"
msgstr "Conectar"

#: ui/Device.qml:102
#, kde-format
msgid "Type:"
msgstr "Tipo:"

#: ui/Device.qml:108
#, kde-format
msgid "%1%"
msgstr "%1 %"

#: ui/Device.qml:112
#, kde-format
msgid "Battery:"
msgstr "Batería:"

#: ui/Device.qml:117 ui/General.qml:42
#, kde-format
msgid "Address:"
msgstr "Dirección:"

#: ui/Device.qml:122
#, kde-format
msgid "Adapter:"
msgstr "Adaptador:"

#: ui/Device.qml:128 ui/General.qml:36
#, kde-format
msgid "Name:"
msgstr "Nombre:"

#: ui/Device.qml:132
#, kde-format
msgid "Trusted"
msgstr "De confianza"

#: ui/Device.qml:138
#, kde-format
msgid "Blocked"
msgstr "Bloqueado"

#: ui/Device.qml:144
#, kde-format
msgid "Send File"
msgstr "Enviar archivo"

#: ui/Device.qml:151
#, kde-format
msgid "Setup NAP Network…"
msgstr "Configurar red NAP..."

#: ui/Device.qml:158
#, kde-format
msgid "Setup DUN Network…"
msgstr "Configurar red DUN..."

#: ui/General.qml:19
#, kde-format
msgid "Settings"
msgstr "Preferencias"

#: ui/General.qml:28
#, kde-format
msgid "Device:"
msgstr "Dispositivo:"

#: ui/General.qml:46
#, kde-format
msgid "Enabled:"
msgstr "Activado:"

#: ui/General.qml:52
#, kde-format
msgid "Visible:"
msgstr "Visible:"

#: ui/General.qml:66
#, kde-format
msgid "On login:"
msgstr "Al iniciar sesión:"

#: ui/General.qml:67
#, kde-format
msgid "Enable Bluetooth"
msgstr "Activar Bluetooth"

#: ui/General.qml:77
#, kde-format
msgid "Disable Bluetooth"
msgstr "Desactivar Bluetooth"

#: ui/General.qml:87
#, kde-format
msgid "Restore previous status"
msgstr "Restaurar el estado anterior"

#: ui/General.qml:106
#, kde-format
msgid "When receiving files:"
msgstr "Al recibir archivos:"

#: ui/General.qml:108
#, kde-format
msgid "Ask for confirmation"
msgstr "Solicitar confirmación"

#: ui/General.qml:117
#, kde-format
msgid "Accept for trusted devices"
msgstr "Aceptar de dispositivos de confianza"

#: ui/General.qml:127
#, kde-format
msgid "Always accept"
msgstr "Aceptar siempre"

#: ui/General.qml:137
#, kde-format
msgid "Save files in:"
msgstr "Guardar archivos en:"

#: ui/General.qml:161 ui/General.qml:172
#, kde-format
msgid "Select folder"
msgstr "Seleccionar carpeta"

#: ui/main.qml:27
#, kde-format
msgctxt "@action: button as in, 'enable Bluetooth'"
msgid "Enabled"
msgstr "Activado"

#: ui/main.qml:43
#, kde-format
msgid "Add New Device…"
msgstr "Añadir nuevo dispositivo..."

#: ui/main.qml:49
#, kde-format
msgid "Configure…"
msgstr "Configurar..."

#: ui/main.qml:88
#, kde-format
msgid "Forget this Device?"
msgstr "¿Olvidar este dispositivo?"

#: ui/main.qml:89
#, kde-format
msgid "Are you sure you want to forget \"%1\"?"
msgstr "¿Seguro que quiere olvidar «%1»?"

#: ui/main.qml:100
#, kde-format
msgctxt "@action:button"
msgid "Forget Device"
msgstr "Olvidar dispositivo"

#: ui/main.qml:107
#, kde-format
msgctxt "@action:button"
msgid "Cancel"
msgstr "Cancelar"

#: ui/main.qml:168
#, kde-format
msgid "No Bluetooth adapters found"
msgstr "No se han encontrado adaptadores Bluetooth"

#: ui/main.qml:169
#, kde-format
msgid "Connect an external Bluetooth adapter"
msgstr "Conectar un adaptador Bluetooth externo"

#: ui/main.qml:178
#, kde-format
msgid "Bluetooth is disabled"
msgstr "Bluetooth está desactivado"

#: ui/main.qml:184
#, kde-format
msgid "Enable"
msgstr "Activar"

#: ui/main.qml:194
#, kde-format
msgid "No devices paired"
msgstr "No hay dispositivos vinculados"

#: ui/main.qml:195
#, kde-kuit-format
msgctxt "@info"
msgid "Click <interface>Add New Device…</interface> to pair some"
msgstr ""
"Pulse <interface>Añadir nuevo dispositivo...</interface> para vincular uno"

#: ui/main.qml:217
#, kde-format
msgid "Connected"
msgstr "Conectado"

#: ui/main.qml:217
#, kde-format
msgid "Available"
msgstr "Disponible"

#: ui/main.qml:253
#, kde-format
msgctxt "@action:button %1 is the name of a Bluetooth device"
msgid "Forget \"%1\""
msgstr "Olvidar «%1»"

#: ui/main.qml:284
#, kde-format
msgid "%1% Battery"
msgstr "%1 % de batería"

#~ msgctxt "This device is a Phone"
#~ msgid "Phone"
#~ msgstr "Teléfono"

#~ msgctxt "This device is a Modem"
#~ msgid "Modem"
#~ msgstr "Módem"

#~ msgctxt "This device is a Computer"
#~ msgid "Computer"
#~ msgstr "Computador"

#~ msgctxt "This device is of type Network"
#~ msgid "Network"
#~ msgstr "Red"

#~ msgctxt "This device is a Headset"
#~ msgid "Headset"
#~ msgstr "Cascos"

#~ msgctxt "This device is a Headphones"
#~ msgid "Headphones"
#~ msgstr "Auriculares"

#~ msgctxt "This device is an Audio/Video device"
#~ msgid "Multimedia Device"
#~ msgstr "Dispositivo multimedia"

#~ msgctxt "This device is a Keyboard"
#~ msgid "Keyboard"
#~ msgstr "Teclado"

#~ msgctxt "This device is a Mouse"
#~ msgid "Mouse"
#~ msgstr "Ratón"

#~ msgctxt "This device is a Joypad"
#~ msgid "Joypad"
#~ msgstr "Mando de videojuegos"

#~ msgctxt "This device is a Graphics Tablet (input device)"
#~ msgid "Tablet"
#~ msgstr "Tableta"

#~ msgctxt "This device is a Peripheral device"
#~ msgid "Peripheral"
#~ msgstr "Periférico"

#~ msgctxt "This device is a Camera"
#~ msgid "Camera"
#~ msgstr "Cámara"

#~ msgctxt "This device is a Printer"
#~ msgid "Printer"
#~ msgstr "Impresora"

#~ msgctxt ""
#~ "This device is an Imaging device (printer, scanner, camera, display, …)"
#~ msgid "Imaging"
#~ msgstr "Dispositivos de imagen"

#~ msgctxt "This device is a Wearable"
#~ msgid "Wearable"
#~ msgstr "Tecnología vestible"

#~ msgctxt "This device is a Toy"
#~ msgid "Toy"
#~ msgstr "Juguete digital"

#~ msgctxt "This device is a Health device"
#~ msgid "Health"
#~ msgstr "Dispositivo de salud"

#~ msgctxt "Type of device: could not be determined"
#~ msgid "Unknown"
#~ msgstr "Desconocido"

#~ msgid "Confirm Deletion of Device"
#~ msgstr "Confirmar borrado de dispositivo"

#~ msgid "Remove"
#~ msgstr "Eliminar"

#~ msgid "Bluetooth"
#~ msgstr "Bluetooth"

#~ msgid "Nicolas Fella"
#~ msgstr "Nicolas Fella"

#~ msgid "Add…"
#~ msgstr "Añadir..."

#~ msgctxt "This device is an Audio device"
#~ msgid "Audio"
#~ msgstr "Sonido"

#~ msgid "Add..."
#~ msgstr "Añadir..."
