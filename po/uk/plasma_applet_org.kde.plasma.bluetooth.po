# Translation of plasma_applet_org.kde.plasma.bluetooth.po to Ukrainian
# Copyright (C) 2015-2020 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2015, 2017, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_org.kde.plasma.bluetooth\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-10-17 01:40+0000\n"
"PO-Revision-Date: 2022-12-07 07:11+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: package/contents/ui/DeviceItem.qml:36
#, kde-format
msgid "Disconnect"
msgstr "Від’єднатися"

#: package/contents/ui/DeviceItem.qml:36
#, kde-format
msgid "Connect"
msgstr "З'єднатися"

#: package/contents/ui/DeviceItem.qml:45
#, kde-format
msgid "Browse Files"
msgstr "Переглянути файли"

#: package/contents/ui/DeviceItem.qml:56
#, kde-format
msgid "Send File"
msgstr "Надіслати файл"

#: package/contents/ui/DeviceItem.qml:112
#, kde-format
msgid "Copy"
msgstr "Копіювати"

#: package/contents/ui/DeviceItem.qml:208
#, kde-format
msgid "Yes"
msgstr "Так"

#: package/contents/ui/DeviceItem.qml:208
#, kde-format
msgid "No"
msgstr "Ні"

#: package/contents/ui/DeviceItem.qml:214
#, kde-format
msgctxt "@label %1 is human-readable adapter name, %2 is HCI"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: package/contents/ui/DeviceItem.qml:222
#, kde-format
msgid "Remote Name"
msgstr "Назва віддаленого пристрою"

#: package/contents/ui/DeviceItem.qml:226
#, kde-format
msgid "Address"
msgstr "Адреса"

#: package/contents/ui/DeviceItem.qml:229
#, kde-format
msgid "Paired"
msgstr "Пов’язано"

#: package/contents/ui/DeviceItem.qml:232
#, kde-format
msgid "Trusted"
msgstr "Надійний"

#: package/contents/ui/DeviceItem.qml:235
#, kde-format
msgid "Adapter"
msgstr "Адаптер"

#: package/contents/ui/DeviceItem.qml:243
#, kde-format
msgid "Disconnecting"
msgstr "Від’єднуємося"

#: package/contents/ui/DeviceItem.qml:243
#, kde-format
msgid "Connecting"
msgstr "Виконується з'єднання"

#: package/contents/ui/DeviceItem.qml:249
#, kde-format
msgid "Connected"
msgstr "З'єднано"

#: package/contents/ui/DeviceItem.qml:256
#, kde-format
msgid "Audio device"
msgstr "Звуковий пристрій"

#: package/contents/ui/DeviceItem.qml:263
#, kde-format
msgid "Input device"
msgstr "Пристрій введення"

#: package/contents/ui/DeviceItem.qml:267
#, kde-format
msgid "Phone"
msgstr "Телефон"

#: package/contents/ui/DeviceItem.qml:274
#, kde-format
msgid "File transfer"
msgstr "Перенесення файлів"

#: package/contents/ui/DeviceItem.qml:277
#, kde-format
msgid "Send file"
msgstr "Надіслати файл"

#: package/contents/ui/DeviceItem.qml:280
#, kde-format
msgid "Input"
msgstr "Введення"

#: package/contents/ui/DeviceItem.qml:283
#, kde-format
msgid "Audio"
msgstr "Звук"

#: package/contents/ui/DeviceItem.qml:286
#, kde-format
msgid "Network"
msgstr "Мережа"

#: package/contents/ui/DeviceItem.qml:290
#, kde-format
msgid "Other device"
msgstr "Інший пристрій"

#: package/contents/ui/DeviceItem.qml:297 package/contents/ui/main.qml:70
#: package/contents/ui/main.qml:80
#, kde-format
msgid "%1% Battery"
msgstr "%1% акумулятора"

#: package/contents/ui/DeviceItem.qml:308
#, kde-format
msgctxt "Notification when the connection failed due to Failed:HostIsDown"
msgid "The device is unreachable"
msgstr "Пристрій недоступний"

#: package/contents/ui/DeviceItem.qml:310
#, kde-format
msgctxt "Notification when the connection failed due to Failed"
msgid "Connection to the device failed"
msgstr "Не вдалося встановити з’єднання із пристроєм"

#: package/contents/ui/DeviceItem.qml:314
#, kde-format
msgctxt "Notification when the connection failed due to NotReady"
msgid "The device is not ready"
msgstr "Пристрій неготовий"

#: package/contents/ui/DeviceItem.qml:348
#, kde-format
msgctxt ""
"@label %1 is human-readable device name, %2 is low-level device address"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: package/contents/ui/FullRepresentation.qml:59
#, kde-format
msgid "Enable"
msgstr "Увімкнути"

#: package/contents/ui/FullRepresentation.qml:147
#, kde-format
msgid "No Bluetooth adapters available"
msgstr "Немає доступних адаптерів Bluetooth"

#: package/contents/ui/FullRepresentation.qml:149
#, kde-format
msgid "Bluetooth is disabled"
msgstr "Bluetooth вимкнено"

#: package/contents/ui/FullRepresentation.qml:151
#, kde-format
msgid "No devices found"
msgstr "Пристроїв не виявлено"

#: package/contents/ui/main.qml:48
#, kde-format
msgid "Bluetooth"
msgstr "Bluetooth"

#: package/contents/ui/main.qml:51
#, kde-format
msgid "Bluetooth is disabled; middle-click to enable"
msgstr "Bluetooth вимкнено; клацніть середньою кнопкою, щоб увімкнути"

#: package/contents/ui/main.qml:55
#, kde-format
msgid "No adapters available"
msgstr "Немає доступних адаптерів"

#: package/contents/ui/main.qml:57
#, kde-format
msgid "Bluetooth is offline"
msgstr "Bluetooth від’єднано"

#: package/contents/ui/main.qml:60
#, kde-format
msgid "Middle-click to disable Bluetooth"
msgstr "Клацніть середньою, щоб вимкнути Bluetooth"

#: package/contents/ui/main.qml:63
#, kde-format
msgid "No connected devices"
msgstr "Немає з’єднаних пристроїв"

#: package/contents/ui/main.qml:68
#, kde-format
msgid "%1 connected"
msgstr "%1 з'єднано"

#: package/contents/ui/main.qml:75
#, kde-format
msgctxt "Number of connected devices"
msgid "%1 connected device"
msgid_plural "%1 connected devices"
msgstr[0] "%1 з’єднаний пристрій"
msgstr[1] "%1 з’єднаних пристрої"
msgstr[2] "%1 з’єднаних пристроїв"
msgstr[3] "%1 з’єднаний пристрій"

#: package/contents/ui/main.qml:137
#, kde-format
msgid "Add New Device…"
msgstr "Додати новий пристрій…"

#: package/contents/ui/main.qml:144 package/contents/ui/Toolbar.qml:32
#, kde-format
msgid "Enable Bluetooth"
msgstr "Увімкнути Bluetooth"

#: package/contents/ui/main.qml:156
#, kde-format
msgid "Configure &Bluetooth…"
msgstr "Нала&штувати Bluetooth…"

#~ msgid ""
#~ "No connected devices\n"
#~ "Middle-click to disable Bluetooth"
#~ msgstr ""
#~ "Немає з'єднаних пристроїв\n"
#~ "Клацніть середньою, щоб вимкнути Bluetooth"

#~ msgid ""
#~ "%1 connected\n"
#~ "Middle-click to disable Bluetooth"
#~ msgstr ""
#~ "З'єднано %1\n"
#~ "Клацніть середньою, щоб вимкнути Bluetooth"

#~ msgid "Bluetooth is Disabled"
#~ msgstr "Bluetooth вимкнено"

#~ msgid "Add New Device..."
#~ msgstr "Додати новий пристрій…"

#~ msgid "Connected devices"
#~ msgstr "З’єднані пристрої"

#~ msgid "Available devices"
#~ msgstr "Доступні пристрої"

#~ msgid "Configure Bluetooth..."
#~ msgstr "Налаштувати Bluetooth…"
