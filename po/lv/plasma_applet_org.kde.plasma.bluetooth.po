# Copyright (C) 2024 This file is copyright:
# This file is distributed under the same license as the bluedevil package.
#
# SPDX-FileCopyrightText: 2024 Toms Trasuns <toms.trasuns@posteo.net>
msgid ""
msgstr ""
"Project-Id-Version: bluedevil\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-10-17 01:40+0000\n"
"PO-Revision-Date: 2024-02-26 15:07+0200\n"
"Last-Translator: Toms Trasuns <toms.trasuns@posteo.net>\n"
"Language-Team: Latvian <kde-i18n-doc@kde.org>\n"
"Language: lv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : "
"2);\n"
"X-Generator: Lokalize 23.08.5\n"

#: package/contents/ui/DeviceItem.qml:36
#, kde-format
msgid "Disconnect"
msgstr "Atvienot"

#: package/contents/ui/DeviceItem.qml:36
#, kde-format
msgid "Connect"
msgstr "Savienot"

#: package/contents/ui/DeviceItem.qml:45
#, kde-format
msgid "Browse Files"
msgstr "Pārlūkot datnes"

#: package/contents/ui/DeviceItem.qml:56
#, kde-format
msgid "Send File"
msgstr "Nosūtīt datni"

#: package/contents/ui/DeviceItem.qml:112
#, kde-format
msgid "Copy"
msgstr "Kopēt"

#: package/contents/ui/DeviceItem.qml:208
#, kde-format
msgid "Yes"
msgstr "Jā"

#: package/contents/ui/DeviceItem.qml:208
#, kde-format
msgid "No"
msgstr "Nē"

#: package/contents/ui/DeviceItem.qml:214
#, kde-format
msgctxt "@label %1 is human-readable adapter name, %2 is HCI"
msgid "%1 (%2)"
msgstr "„%1“ (%2)"

#: package/contents/ui/DeviceItem.qml:222
#, kde-format
msgid "Remote Name"
msgstr "Attālais nosaukums"

#: package/contents/ui/DeviceItem.qml:226
#, kde-format
msgid "Address"
msgstr "Adrese"

#: package/contents/ui/DeviceItem.qml:229
#, kde-format
msgid "Paired"
msgstr "Sapārots"

#: package/contents/ui/DeviceItem.qml:232
#, kde-format
msgid "Trusted"
msgstr "Uzticams"

#: package/contents/ui/DeviceItem.qml:235
#, kde-format
msgid "Adapter"
msgstr "Adapteris"

#: package/contents/ui/DeviceItem.qml:243
#, kde-format
msgid "Disconnecting"
msgstr "Atvienojas"

#: package/contents/ui/DeviceItem.qml:243
#, kde-format
msgid "Connecting"
msgstr "Savienojas"

#: package/contents/ui/DeviceItem.qml:249
#, kde-format
msgid "Connected"
msgstr "Savienots"

#: package/contents/ui/DeviceItem.qml:256
#, kde-format
msgid "Audio device"
msgstr "Audio ierīce"

#: package/contents/ui/DeviceItem.qml:263
#, kde-format
msgid "Input device"
msgstr "Ievades ierīce"

#: package/contents/ui/DeviceItem.qml:267
#, kde-format
msgid "Phone"
msgstr "Tālrunis"

#: package/contents/ui/DeviceItem.qml:274
#, kde-format
msgid "File transfer"
msgstr "Datņu nosūtīšana"

#: package/contents/ui/DeviceItem.qml:277
#, kde-format
msgid "Send file"
msgstr "Nosūtīt datni"

#: package/contents/ui/DeviceItem.qml:280
#, kde-format
msgid "Input"
msgstr "Ievade"

#: package/contents/ui/DeviceItem.qml:283
#, kde-format
msgid "Audio"
msgstr "Audio"

#: package/contents/ui/DeviceItem.qml:286
#, kde-format
msgid "Network"
msgstr "Tīkls"

#: package/contents/ui/DeviceItem.qml:290
#, kde-format
msgid "Other device"
msgstr "Cita ierīce"

#: package/contents/ui/DeviceItem.qml:297 package/contents/ui/main.qml:70
#: package/contents/ui/main.qml:80
#, kde-format
msgid "%1% Battery"
msgstr "%1% baterija"

#: package/contents/ui/DeviceItem.qml:308
#, kde-format
msgctxt "Notification when the connection failed due to Failed:HostIsDown"
msgid "The device is unreachable"
msgstr "Ierīce nav sasniedzama"

#: package/contents/ui/DeviceItem.qml:310
#, kde-format
msgctxt "Notification when the connection failed due to Failed"
msgid "Connection to the device failed"
msgstr "Savienojums ar šo ierīci nav izdevies"

#: package/contents/ui/DeviceItem.qml:314
#, kde-format
msgctxt "Notification when the connection failed due to NotReady"
msgid "The device is not ready"
msgstr "Ierīce nav gatava"

#: package/contents/ui/DeviceItem.qml:348
#, kde-format
msgctxt ""
"@label %1 is human-readable device name, %2 is low-level device address"
msgid "%1 (%2)"
msgstr "„%1“ (%2)"

#: package/contents/ui/FullRepresentation.qml:59
#, kde-format
msgid "Enable"
msgstr "Ieslēgt"

#: package/contents/ui/FullRepresentation.qml:147
#, kde-format
msgid "No Bluetooth adapters available"
msgstr "Nav pieejamu „Bluetooth“ adapteru"

#: package/contents/ui/FullRepresentation.qml:149
#, kde-format
msgid "Bluetooth is disabled"
msgstr "„Bluetooth“ ir izslēgts"

#: package/contents/ui/FullRepresentation.qml:151
#, kde-format
msgid "No devices found"
msgstr "Ierīces nav atrastas"

#: package/contents/ui/main.qml:48
#, kde-format
msgid "Bluetooth"
msgstr "Bluetooth"

#: package/contents/ui/main.qml:51
#, kde-format
msgid "Bluetooth is disabled; middle-click to enable"
msgstr "„Bluetooth“ ir izslēgts. Ieslēdziet ar peles vidējo pogu"

#: package/contents/ui/main.qml:55
#, kde-format
msgid "No adapters available"
msgstr "Nav pieejamu adapteru"

#: package/contents/ui/main.qml:57
#, kde-format
msgid "Bluetooth is offline"
msgstr "„Bluetooth“ ir bezsaistē"

#: package/contents/ui/main.qml:60
#, kde-format
msgid "Middle-click to disable Bluetooth"
msgstr "Izslēdziet ar peles vidējo pogu"

#: package/contents/ui/main.qml:63
#, kde-format
msgid "No connected devices"
msgstr "Nav pievienotu ierīču"

#: package/contents/ui/main.qml:68
#, kde-format
msgid "%1 connected"
msgstr "Ierīce „%1“ ir pievienota"

#: package/contents/ui/main.qml:75
#, kde-format
msgctxt "Number of connected devices"
msgid "%1 connected device"
msgid_plural "%1 connected devices"
msgstr[0] "%1 pievienota ierīce"
msgstr[1] "%1 pievienotas ierīces"
msgstr[2] "%1 pievienotu ierīču"

#: package/contents/ui/main.qml:137
#, kde-format
msgid "Add New Device…"
msgstr "Pievienot jaunu ierīci..."

#: package/contents/ui/main.qml:144 package/contents/ui/Toolbar.qml:32
#, kde-format
msgid "Enable Bluetooth"
msgstr "Ieslēgt „Bluetooth“"

#: package/contents/ui/main.qml:156
#, kde-format
msgid "Configure &Bluetooth…"
msgstr "Konfigurēt „&Bluetooth“..."
